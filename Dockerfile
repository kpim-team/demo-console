FROM mcr.microsoft.com/dotnet/core/runtime:2.1 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:2.1 AS build
WORKDIR /src
COPY ["demo-console.csproj", "./"]
RUN dotnet restore "./demo-console.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "demo-console.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "demo-console.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "demo-console.dll"]
