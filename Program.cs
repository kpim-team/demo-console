﻿using System;
using System.Threading;

namespace demo_console
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true){
                Console.WriteLine("[{0:HH:mm:ss}] - Rodando...", DateTime.Now);
                Thread.Sleep(3000);
            }
        }
    }
}
